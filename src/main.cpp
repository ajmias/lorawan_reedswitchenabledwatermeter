/******************************LoRaWAN watermeter********************************/

#include <Arduino.h>
#include <Wire.h>
#define loraSerial Serial1
#define debugSerial Serial
///////////////////////////////////
#include <lmic.h>
#include <hal/hal.h>
#include "lorawanConfig.h"
///////////////////////////////////
#include <EnableInterrupt.h>
///////////////////////////////////
#include <LowPower.h>
#include <avr/wdt.h>
///////////////////////////////////
#include <EEPROM.h>
///////////////////////////////////

float input_volt = 0.0;
float temp=0.0;
float input_volt1 = 0.0;
float temp1=0.0;

static const int MaxCount = 4;
int currentCount = 0;
long sum = 0;
long total = 0;
long previous_value;
#define ARDUINOPIN 7
#define TRIGGER 8

unsigned char a=0,f=0,co=0,tam=0;
//volatile uint16_t InterruptCount = 0;
unsigned char x01=0,x1=0; //x1 will be set with the tens digit of the first reading
long int x=0;  //x is water reading in cubic meter,x1 is one point after decimal,x01 is two point after decimal. 
unsigned char lit1=0;

// use low power sleep; comment next line litto not use low power sleep
#define SLEEP
// show debug statements; comment next line to disable debug statements
#define DEBUG

void switch1_interruptFunction();
void switch2_interruptFunction();
void read_eeprom();
void write_eeprom();
void read_battery_voltage();

void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }

struct data {
  short int Voltage;
  char x01;
  char x1;
  long int x;
  char tamp;
  char error;
}mydata;

static osjob_t sendjob;
const unsigned TX_INTERVAL = 30;
const lmic_pinmap lmic_pins = {
    .nss = 6,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 5,
    .dio = {2,3,4},
};

unsigned char t1=0;
bool next = false;
void onEvent (ev_t ev) {
 #ifdef DEBUG
     Serial.println(F("Enter onEvent"));
 #endif

  switch (ev) {
 #ifdef DEBUG
    case EV_SCAN_TIMEOUT:
      Serial.println(F("EV_SCAN_TIMEOUT"));
      break;
    case EV_BEACON_FOUND:
      Serial.println(F("EV_BEACON_FOUND"));
      break;
    case EV_BEACON_MISSED:
      Serial.println(F("EV_BEACON_MISSED"));
      break;
    case EV_BEACON_TRACKED:
      Serial.println(F("EV_BEACON_TRACKED"));
      break;
    case EV_JOINING:
      Serial.println(F("EV_JOINING"));
      break;
    case EV_JOINED:
      Serial.println(F("EV_JOINED"));
      break;
    case EV_RFU1:
      Serial.println(F("EV_RFU1"));
      break;
    case EV_JOIN_FAILED:
      Serial.println(F("EV_JOIN_FAILED"));
      break;
    case EV_REJOIN_FAILED:
      Serial.println(F("EV_REJOIN_FAILED"));
      break;
 #endif
    case EV_TXCOMPLETE:
      Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
      if (LMIC.dataLen)
      {
        //data received in rx slot after tx
        Serial.print(F("Data Received: "));
        Serial.write(LMIC.frame + LMIC.dataBeg, LMIC.dataLen);
        Serial.println();
      }
      // Schedule next transmission
      // os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL), do_send);
      next = true;
    break;

 #ifdef DEBUG
    case EV_LOST_TSYNC:
      Serial.println(F("EV_LOST_TSYNC"));
      break;
    case EV_RESET:
      Serial.println(F("EV_RESET"));
      break;
    case EV_RXCOMPLETE:
      // data received in ping slot
      Serial.println(F("EV_RXCOMPLETE"));
      break;
    case EV_LINK_DEAD:
      Serial.println(F("EV_LINK_DEAD"));
      break;
    case EV_LINK_ALIVE:
      Serial.println(F("EV_LINK_ALIVE"));
      break;
    default:
      Serial.println(F("Unknown event"));
      break;
#endif
  }
#ifdef DEBUG
  Serial.println(F("Leave onEvent"));
#endif
}

void do_send(osjob_t* j)
{
  read_battery_voltage();
  delay(100);
  if(LMIC.opmode & OP_TXRXPEND){
    Serial.println(F("OP_TXRXPEND, not sending"));
  }
  else{
    LMIC_setTxData2(1, (unsigned char *)&mydata, sizeof(mydata) - 1, 0);
    Serial.println(F("Packet queued"));
  }
}

void setup()
{
  #ifdef DEBUG
    Serial.begin(9600);
    Serial.println("Initialize WaterMeter");
  #endif  
    pinMode(ARDUINOPIN, INPUT); 
    pinMode(TRIGGER, INPUT);
    enableInterrupt(TRIGGER, switch2_interruptFunction, RISING);
  #ifdef DEBUG
    Serial.println(F("Enter setup"));
  #endif

    // enable interrupt  
    digitalWrite(A0, HIGH);
    delay(10);
    enableInterrupt(ARDUINOPIN, switch1_interruptFunction, RISING);

    // read current values from eeprom
    read_eeprom();
    
    // LMIC init    
    os_init();
    LMIC_reset();
    #ifdef PROGMEM
        uint8_t appskey[sizeof(APPSKEY)];
        uint8_t nwkskey[sizeof(NWKSKEY)];
        memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
        memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
        LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);
    #else
        LMIC_setSession (0x1, DEVADDR, NWKSKEY, APPSKEY);
    #endif

    #if defined(CFG_eu868)
        LMIC_setupChannel(0, 865062500, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
        LMIC_setupChannel(1, 865402500, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);      // g-band
        LMIC_setupChannel(2, 865985000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    #elif defined(CFG_us915)
        LMIC_selectSubBand(1);
    #endif

    LMIC_setLinkCheckMode(0);
    LMIC.dn2Dr = DR_SF9;
    LMIC_setDrTxpow(DR_SF12, 14);
    // Start job
    do_send(&sendjob);
}

void loop()
{
  write_eeprom();
  delay(300);
  if(total>=previous_value+1000)
  {
    //next == false;
    previous_value = total;
    do_send(&sendjob);
    next == false;
  }
  delay(300);

  extern volatile unsigned long timer0_overflow_count;
    
  if (next == false) {
    os_runloop_once();
    }
  else{
    #ifdef DEBUG
      Serial.print(F("Enter sleeping forever "));
      Serial.flush(); // give the serial print chance to complete
    #endif

    LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
    cli();
    timer0_overflow_count += 8 * 64 * clockCyclesPerMicrosecond();
    sei();

    #ifdef DEBUG
      Serial.println(F("Sleep complete"));
    #endif
    //next == false;
    }  
}

/*ISR
fn name: switch1_interruptFunction
brief: serves the interupt from switch 2*/
void switch2_interruptFunction() 
{
  if(f==2)
  {
    a++;
    co=0;
    f=1; 
  }
  f=1;
  disableInterrupt(TRIGGER);
  enableInterrupt(ARDUINOPIN, switch1_interruptFunction, RISING);
}

/*ISR
fn name: switch1_interruptFunction
brief: serves the interupt from switch 1*/
void switch1_interruptFunction()
{
  if(f==1)
  {
    f=2;
  }
  disableInterrupt(ARDUINOPIN);
  enableInterrupt(TRIGGER, switch2_interruptFunction, RISING);
}

/*Read battery voltage 
fn name: read_battery_voltage()
brief: it reads and returns the battery capacity in mV connected to an analog pin*/
void read_battery_voltage()
{
  float analogvalue;
  for (byte  i = 0; i < 10; i++)
  {
    analogvalue += analogRead(A3);
    //Serial.print("analogvalue= ");               
    //Serial.println(analogRead(A3));
  }
  analogvalue=analogvalue/10;
  temp = ((analogvalue* 3.3)/1024);  //ADC voltage*Ref. Voltage/1024
  float temp1=temp*2;
  float sum=0;
  double avg=0;
  for (byte  i = 0; i < 4; i++)
  {
    sum += temp1;
  }
  avg = sum / 4;
  int tp=avg*100;
  #ifdef DEBUG
    Serial.print("Voltage= ");               
    Serial.print(avg);
    Serial.println("V");
  #endif
  mydata.Voltage=tp;
  mydata.x01=x01; mydata.x1=x1; mydata.x=x;
  tp=0;
}

/*write to eeprom
fn name: write_eeprom()
brief: write/store values into EEPROM*/
void write_eeprom() 
{
  x01 = a;
  EEPROM.put(10, x01);
  t1 = a;
  if (x01 > 9) {
    x1++;
    EEPROM.put(50, x1);
    x01 = 0;
    a = 0;
  }
  if (x1 > 9) {
    x++;
    EEPROM.put(90, x);
    x1 = 0;
  }
  total = ((x * 1000) + (x1 * 100) + (x01 * 10));
  #ifdef DEBUG
    Serial.print("x = ");
    Serial.println(x);
    Serial.print("x1 = ");
    Serial.println(x1);
    Serial.print("x01 = ");
    Serial.println(x01);
    Serial.print("Total=");
    Serial.println(total);
  #endif
}

/*read from eeprom
fn name: read_eeprom()
brief: read values from EEPROM*/
void read_eeprom()
{
  EEPROM.get(10, x01);
  EEPROM.get(50, x1);
  EEPROM.get(90, x);
  a=x01;
  lit1=a;
  delay(500);
}
